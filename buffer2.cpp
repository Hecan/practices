// Excercise.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <new.h>
#include <stdlib.h>  
#include <stdio.h>  
#include <time.h>  
#include <vector>

typedef unsigned char byte;

class Data {
private:

	byte *			data_;
	uint32_t		offset_to_start;
	uint32_t		length_;
	uint32_t		current_length_;

public:

	Data::Data()
		: data_(nullptr)
		, length_(0)
		, current_length_(0)
		, offset_to_start(0)
	{}

	Data::~Data()
	{
		if (data_)
			delete[] data_;
	}

	void put_data(byte *data, uint32_t length)
	{

		if (length_ < current_length_ + length)
		{
			std::cout << "increasing buffer memory" << std::endl;
			length_ = current_length_ + length;
			byte *data_new = new byte[length_];
			memcpy(data_new, data_, current_length_);

			if (data_) delete[] data_;

			data_ = data_new;
		}

		memcpy(data_ + current_length_, data, length);
		current_length_ += length;
	}

	byte *return_pointer_to_data(uint32_t& length)
	{
		byte * return_ptr = data_ + offset_to_start;
		
		if (length > current_length_)
		{
			length = current_length_;
			current_length_ = 0;
			offset_to_start = 0;
			return return_ptr;
		}

		offset_to_start += length;
		current_length_ -= length;
		return return_ptr;
	}

	uint32_t current_data_length() const {
		return current_length_;
	}
};

std::uint32_t gen_num()
{
	return (double)rand() / (RAND_MAX + 1) * (10000000 - 1000) + 1000;
}

int main()
{
	byte* my_data = new byte[10000000];

	srand((unsigned)time(NULL));

	Data data_object;
	uint32_t length;
	for (auto i = 1; i < 10000; i++)
	{
		data_object.put_data(my_data, gen_num());

		if (i % 10 == 0)
		{
			while (data_object.current_data_length() > 0)
			{
				length = gen_num() / 10;
				data_object.return_pointer_to_data(length);
			}
		}
	}


	system("pause");

	return 0;
}
