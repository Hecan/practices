#pragma once

#include <stdint.h>

enum colorspace_format
{
	UYVY = 1,
	YUYV = 2,
	RGB = 3
};

class colorspace_converter
{
private:
	int y, u, v;
	int C, D, E;
	int r, g, b, a=0;

	uint8_t *source_buffer_;
	uint8_t *dest_buffer_;

	uint64_t dest_buffer_size;

	uint32_t width_;
	uint32_t height_;

	enum colorspace_format source_format_;
	enum colorspace_format dest_format_;

public:
	colorspace_converter();
	~colorspace_converter();

	void initialize(
		uint32_t mconcept_width_,
		uint32_t mconcept_height_,
		enum colorspace_format source_format,
		enum colorspace_format dest_format);
	void set_source_buffer(uint8_t *source_buffer);
	void UYVYtoRGB();

	BYTE *convert();

	uint64_t get_converted_buffer_size() { return dest_buffer_size; }

};