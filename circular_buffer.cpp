/* Circular Buffer */

#include "stdafx.h"
#include <iostream>
#include <stdlib.h>  
#include <stdio.h>  
#include <time.h>

typedef unsigned char byte;

class Buffer {
private:

	byte *			data_;
	uint32_t		read_pos;
	uint32_t		write_pos;
	uint32_t		length_;

public:

	Buffer::Buffer()
		: data_(nullptr)
		, read_pos(0)
		, write_pos(0)
		, length_(0)
	{}

	Buffer::~Buffer()
	{
		if (data_)
		{
			delete[] data_;
		}
	}

	void put_data(byte *data, uint32_t length)
	{
		if (write_pos >= read_pos)
		{
			if (length_ - write_pos >= length)
			{
				memcpy(data_ + write_pos, data, length);
				write_pos += length;
			}
			else if (length_ - write_pos < length)
			{
				uint32_t data_put = length_ - write_pos;
				uint32_t data_left = length - data_put;
				if (data_left <= read_pos)
				{
					memcpy(data_ + write_pos, data, data_put);
					memcpy(data_, data + data_put, data_left);
					write_pos = data_left;
				}
				else
				{
					std::cout << "Increasing buffer memory1" << std::endl;
					length_ = write_pos - read_pos + length;
					byte *new_data = new byte[length_];
					memmove(new_data, data_ + read_pos, write_pos - read_pos);

					if (data_)
					{
						delete[] data_;
					}

					write_pos -= read_pos;
					read_pos = 0;
					
					memcpy(new_data + write_pos, data, length);
					data_ = new_data;
					write_pos += length;
				}
			}
		}
		else if (write_pos < read_pos)
		{
			if (read_pos - write_pos >= length)
			{
				memcpy(data_ + write_pos, data, length);
				write_pos += length;
			}
			else if (read_pos - write_pos < length)
			{
				std::cout << "Increasing buffer memory2" << std::endl;
				uint32_t data_lenght = length_ - read_pos + write_pos;
				uint32_t new_length = data_lenght + length;
				byte *new_data = new byte[new_length];

				memcpy(new_data + length_ - read_pos, data_, write_pos);
				memcpy(new_data, data_ + read_pos, length_ - read_pos);
				write_pos = data_lenght;

				if (data_)
				{
					delete[] data_;
				}

				memcpy(new_data + write_pos, data, length);
				data_ = new_data;
				write_pos += length;
				read_pos = 0;
				length_ = new_length;
			}
		}
	}

	byte *return_pointer_to_data(uint32_t& length)
	{
		if (write_pos < read_pos)
		{
			if (length_ - read_pos < length)
			{
				length = length_ - read_pos;
				byte *return_pos = data_ + read_pos;
				read_pos = 0;
				return return_pos;
			}
			else
			{
				byte *return_pos = data_ + read_pos;
				read_pos += length;
				return return_pos;
			}
			
		}
		else if (write_pos > read_pos)
		{
			if (write_pos - read_pos > length)
			{
				byte *return_pos = data_ + read_pos;
				read_pos += length;
				return return_pos;
			}
			else
			{
				byte *return_pos = data_ + read_pos;
				length = write_pos - read_pos;
				read_pos = write_pos;
				return return_pos;
			}
		}
	}
};

std::uint32_t gen_num()
{
	return (double)rand() / (RAND_MAX + 1) * (10000000 - 1000) + 1000;
}

int main()
{
	byte* my_data = new byte[10000000];

	srand((unsigned)time(NULL));

	Buffer circular_buffer;
	uint32_t buffer_length;
	for (auto i = 1; i < 10000; i++)
	{
		circular_buffer.put_data(my_data, gen_num());

		if (i % 5 == 0)
		{
			buffer_length = gen_num()*10;
			circular_buffer.return_pointer_to_data(buffer_length);
		}
	}


	system("pause");

	return 0;
}
