#include "stdafx.h"
#include "colorspace_converter.h"

colorspace_converter::colorspace_converter()
{
}

colorspace_converter::~colorspace_converter()
{
	if (dest_buffer_) delete[] dest_buffer_;
}

void colorspace_converter::initialize(
	uint32_t mconcept_width_,
	uint32_t mconcept_height_,
	colorspace_format source_format,
	colorspace_format dest_format)
{
	width_ = mconcept_width_;
	height_ = mconcept_height_;
	source_format_ = source_format;
	dest_format_ = dest_format;

	dest_buffer_size = width_ * height_ * 4;
	dest_buffer_ = new uint8_t[dest_buffer_size];

}

void colorspace_converter::set_source_buffer(uint8_t *source_buffer)
{
	source_buffer_ = source_buffer;
}

void colorspace_converter::UYVYtoRGB()
{
	for (uint32_t i = 0, j = 0; i < width_ * height_ * 4; i += 8, j += 4) {
		
		// First pixel
		y = source_buffer_[j + 1];
		u = source_buffer_[j];
		v = source_buffer_[j + 2];

		C = y - 16;
		D = v - 128;
		E = u - 128;

		r = (298 * C + 409 * E + 128) / 256;
		g = (298 * C - 100 * D - 208 * E + 128) / 256;
		b = (298 * C + 516 * D + 128) / 256;

		// Prevents colour distortions in rgb image
		if (r < 0) r = 0;
		else if (r > 255) r = 255;
		if (g < 0) g = 0;
		else if (g > 255) g = 255;
		if (b < 0) b = 0;
		else if (b > 255) b = 255;

		dest_buffer_[i]		= (BYTE)r;
		dest_buffer_[i + 1] = (BYTE)g;
		dest_buffer_[i + 2] = (BYTE)b;
		dest_buffer_[i + 3] = (BYTE)a;

		// Second pixel
		y = source_buffer_[j + 3];

		C = y - 16;	

		r = (298 * C + 409 * E + 128) / 256;
		g = (298 * C - 100 * D - 208 * E + 128) / 256;
		b = (298 * C + 516 * D + 128) / 256;

		// Prevents colour distortions in rgb image
		if (r < 0) r = 0;
		else if (r > 255) r = 255;
		if (g < 0) g = 0;
		else if (g > 255) g = 255;
		if (b < 0) b = 0;
		else if (b > 255) b = 255;

		dest_buffer_[i + 4] = (BYTE)r;
		dest_buffer_[i + 5] = (BYTE)g;
		dest_buffer_[i + 6] = (BYTE)b;
		dest_buffer_[i + 7] = (BYTE)a;
	}
}

BYTE* colorspace_converter::convert()
{
	switch (source_format_)
	{
	case UYVY:
		UYVYtoRGB();
		break;
	case YUYV:
		// TODO Implement this function if source format comes out YUYV
		// YUYVtoRGB(); 
		break;
	default:
		break;
	}

	return dest_buffer_;
}
